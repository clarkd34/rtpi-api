const routes = require('express').Router();

routes.get('/', (req, res) => {
  res.send({
    message: 'Tram services connected'
  });
});

/* Export routes for tram services */
module.exports = routes;