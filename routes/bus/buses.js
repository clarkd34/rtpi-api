const routes = require('express').Router();

routes.get('/', (req, res) => {
  res.send({
    message: 'Bus services connected'
  });
});

/* Export routes for bus services */
module.exports = routes;