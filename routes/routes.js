const routes = require('express').Router();
const railRoutes = require('./rail/trains');
const busRoutes = require('./bus/buses');
const tramRoutes = require('./tram/trams');

routes.use('/rail', railRoutes);
routes.use('/bus', busRoutes);
routes.use('/tram', tramRoutes);

module.exports = routes;