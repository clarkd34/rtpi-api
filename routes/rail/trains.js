const router = require('express').Router();
const moment = require('moment');
const trains = require('../../services/trains');

/* Bind routes to router */
router.get('/stations', (req, res) => {
  const options = {
    type: req.query.type,
    name: req.query.name
  };

  trains.getStations(options)
    .then(data => res.send(data))
    .catch(e => {
      console.log(e);
      res.status(500).send({
        message: 'An error occurred while fetching station data'
      });
    });
});

router.get('/station', (req, res) => {
  if(!req.query.name && !req.query.code) {
    res.status(400).send({
      message: 'Missing required parameter: At least one of \'name\' or \'code\' is required'
    });
  }

  trains.getStationActivity({
      name: req.query.name,
      code: req.query.code,
      mins: req.query.mins
    })
    .then(data => res.send(data))
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: 'An error occurred while fetching station activity data'
      });
    })
});

router.get('/trains', (req, res) => {
  trains.getActiveTrains(req.query.type)
    .then(data => res.send(data))
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: 'An error occurred while fetching active train data'
      });
    });
});

router.get('/train', (req, res) => {
  if(!req.query.code) {
    res.status(400).send({
      message: 'Missing required parameter: \'code\''
    });
  }

  const code = req.query.code;
  const date = req.query.date ? moment(req.query.date, 'YYYY-MM-DD') : moment();

  trains.getTrainDetails(code, date)
    .then(data => res.send(data))
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: 'An error occurred while fetching train specific data'
      });
    })
});

/* Export routes for train service */
module.exports = router;
