const express = require('express');
const routes = require('../routes/routes');

const port = process.env.PORT || 3003;
const app = express();

app.use('/', routes);

app.listen(port, () => console.log('Server is up!'));
