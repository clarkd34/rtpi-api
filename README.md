# RTPI for Irish Transport Services

## Table of Contents
1. [Getting Started]
2. [APIs]
  * [Irish Rail]
      * [`/stations`]
      * [`/station`]
      * [`/trains`]
      * [`/train`]
  * Dublin Bus (Coming soon)
  * Luas (Coming soon)
 
## Getting Started
To get the server up and running: 
```shell
# Clone the repository
$ git clone https://gitlab.com/clarkd34/rtpi-api.git
# Navigate into the new directory
$ cd rtpi-api
# Install dependencies
$ yarn install
# Start server
$ node server/app.js
```
 
## APIs
### Irish Rail
 
[Getting Started]:#getting-started
[APIs]:#apis
[Irish Rail]:#irish-rail