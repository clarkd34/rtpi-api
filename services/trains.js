const axios = require('axios');
const parseString = require('xml2js').parseString;

/* Get all stations, optionally by type */
const getStations = async ({ type, name }) => {
  let url = 'http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML';
  const params = {};

  if(type) {
    url += '_WithStationType';
    params.StationType = type;
  }

  try {
    const xml = await axios.get(url, { params });
    let stations = [];

    parseString(xml.data, { explicitRoot: false, ignoreAttrs: true }, (err, json) => {
      if (err) {
        throw new Error(err);
      }

      stations = json.objStation || [];
      stations
        .map(station => {
          for (let prop in station) {
            if (station.hasOwnProperty(prop)) {
              station[prop] = station[prop][0];
            }
          }
          return station;
        })
        .filter(station => !name ||
          station.StationDesc.toUpperCase().includes(name.toUpperCase()));
    });

    return stations;
  }
  catch (e) {
    throw new Error(e);
  }
};

/* Get information for a specific station */
const getStationActivity = async ({ name, code,  mins }) => {
  let baseUrl = 'http://api.irishrail.ie/realtime/realtime.asmx/';
  const byName = 'getStationDataByNameXML';
  const byCode = 'getStationDataByCodeXML';
  const withMins = '_WithNumMins';
  const params = {};

  if(name) {
    baseUrl += byName;
    params.StationDesc = name;
  }
  else {
    baseUrl += byCode;
    params.StationCode = code;
  }

  if(mins) {
    baseUrl += withMins;
    params.NumMins = mins;
  }

  const xml = await axios.get(baseUrl, { params });
  let events = [];

  parseString(xml.data, { explicitRoot: false, ignoreAttrs: true }, (err, json) => {
    if (err) {
      throw new Error(err);
    }

    events = json.objStationData || [];
    events
      .map(event => {
        for (const prop in event) {
          if (event.hasOwnProperty(prop)) {
            event[prop] = event[prop][0];
          }
        }
        return event;
      })
      .filter(event => !!mins || event.Duein <= mins)
  });

  return events;
};

/* Get all active trains (including trains departing within 10 mins) */
const getActiveTrains = async type => {
  let url = 'http://api.irishrail.ie/realtime/realtime.asmx/getCurrentTrainsXML';
  const params = {};

  if (type) {
    url += '_WithTrainType';
    params.TrainType = type;
  }

  const xml = await axios.get(url, { params });
  let trains = [];

  parseString(xml.data, { explicitRoot: false, ignoreAttrs: true }, (err, json) => {
    if (err) {
      throw new Error(err);
    }

    trains = json.objTrainPositions || [];
    for(const station of trains) {
      for (const prop in station) {
        if (station.hasOwnProperty(prop)) {
          station[prop] = station[prop][0];
        }
      }
    }
  });

  return trains;
};

/* Get movement data for a specific train code on a given date */
const getTrainDetails = async (code, date) => {
  let url = 'http://api.irishrail.ie/realtime/realtime.asmx/getTrainMovementsXML';
  const params = {
    TrainId: code,
    TrainDate: date.format('DD MMM YYYY')
  };

  const xml = await axios.get(url, { params });
  let details = [];

  parseString(xml.data, { explicitRoot: false, ignoreAttrs: true }, (err, json) => {
    if (err) {
      throw new Error(err);
    }

    details = json.objTrainMovements || [];
    for (const detail of details) {
      for (const prop in detail) {
        if (detail.hasOwnProperty(prop)) {
          detail[prop] = detail[prop][0];
        }
      }
    }
  });

  return details;
};

/* Export train services */
module.exports = {
  getActiveTrains,
  getStationActivity,
  getStations,
  getTrainDetails
};